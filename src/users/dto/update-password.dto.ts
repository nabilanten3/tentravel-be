import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString, Matches, MaxLength, MinLength } from "class-validator";

export class UpdatePasswordDto {
    @ApiProperty()
    @IsString()
    @MinLength(6)
    @MaxLength(20)
    // @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {message: 'password too weak'})
    password: string;

    @ApiProperty()
    @IsString()
    @MinLength(6)
    @MaxLength(20)
    @IsNotEmpty()
    // @match
    confirmPassword: string


    @IsNotEmpty({ groups: ['passwordMatch'] })
    passwordMatch(): boolean {
        if (this.password && this.confirmPassword && this.password !== this.confirmPassword) {
            return false;
        }
        return true;
    }
}