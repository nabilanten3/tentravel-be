import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { EntityNotFoundError, Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { CreatePelangganDto } from './dto/create-pelanggan.dto';
import { UpdatePelangganDto } from './dto/update-pelanggan.dto';
import { Pelanggan } from './entities/pelanggan.entity';

@Injectable()
export class PelangganService {
  constructor(
    @InjectRepository(Pelanggan)
    private pelangganRepository: Repository<Pelanggan>,
    @InjectRepository(User)
    private userRepository: Repository<User>
  ){}
  async create(data: any, userId: string) {
    const checkPhone = await this.pelangganRepository.findOne({
      where: {
        phone: data.phone,
      }
    });

    if(checkPhone) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_ACCEPTABLE,
          message: 'account is already exist'
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    console.log(data);
    console.log(userId);
    
    

    const pelanggan = new Pelanggan()
    pelanggan.name = data.name
    pelanggan.phone = data.phone
    pelanggan.address = data.address
    pelanggan.user = await this.userRepository.findOne({where: {id: userId}})

    console.log(pelanggan);
    
    
    const result = await this.pelangganRepository.insert(pelanggan)
    return this.pelangganRepository.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }

  async findAll (query: PaginateQuery): Promise<Paginated<Pelanggan>>{
    return paginate(query, this.pelangganRepository, {
      sortableColumns: ['name'],
      defaultSortBy: [['name', 'ASC']],
      searchableColumns: ['name'],
      defaultLimit: 5,
      relations: ['user']
    })
  }


  async findOne(id: string) {
    try {
      return await this.pelangganRepository.findOneOrFail({
        where: {
          id,
        },
        relations: ['user']
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async update(id: string, updatePelangganDto:UpdatePelangganDto) {
    try {
      await this.pelangganRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    const pelanggan = new Pelanggan();
    pelanggan.name = updatePelangganDto.name
    pelanggan.address = updatePelangganDto.address
    pelanggan.phone = updatePelangganDto.phone
    pelanggan.image = updatePelangganDto.image
    await this.pelangganRepository.update(id, pelanggan);

    return this.pelangganRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  async remove(id: string) {
    try {
      await this.pelangganRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.pelangganRepository.delete(id);
  }
}
