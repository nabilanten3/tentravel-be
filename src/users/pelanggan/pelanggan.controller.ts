import { Controller, Get, Post, Body, Patch, Param, Delete, Put, HttpStatus, ParseUUIDPipe } from '@nestjs/common';
import { PelangganService } from './pelanggan.service';
import { CreatePelangganDto } from './dto/create-pelanggan.dto';
import { UpdatePelangganDto } from './dto/update-pelanggan.dto';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { Pelanggan } from './entities/pelanggan.entity';

@Controller('pelanggan')
export class PelangganController {
  constructor(private readonly pelangganService: PelangganService) {}

  @Post()
  async create(@Body() createPelangganDto: CreatePelangganDto, userId: string) {
    try{
      return {
          data: await this.pelangganService.create(createPelangganDto, userId),
          statusCode: HttpStatus.CREATED,
          message: 'success create account',
      };
  } catch(e){
      console.log(e);
      
      return e;
  }
  }

  @Get()
  async getAll(@Paginate() query: PaginateQuery): Promise<Paginated<Pelanggan>>{
    try{
      return await this.pelangganService.findAll(query)
    } catch(e){
      console.log(e);

    }
  }

  @Get(':id')
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.pelangganService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put(':id')
  async update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updatePelangganDto: UpdatePelangganDto,
  ) {
    return {
      data: await this.pelangganService.update(id, updatePelangganDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete(':id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.pelangganService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
