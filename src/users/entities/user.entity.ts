import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  VersionColumn,
  CreateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Karyawan } from '../karyawan/entities/karyawan.entity';
import { Pelanggan } from '../pelanggan/entities/pelanggan.entity';

export enum Level {
  ADMIN = "admin",
  OP = "operator",
  PELANGGAN = "pelanggan",
  OWNER = "pemilik"
}

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    unique: true,
  })
  email: string;

  @Column()
  password: string;

  @Column()
  salt: string;

  @Column({ default: true, select: false})
  isActive: boolean;

  @Column()
  level: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
  deletedAt: Date;

  @VersionColumn({select: false})
  version: number;
}
