import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { User } from './entities/user.entity';
import { AuthModule } from 'src/auth/auth.module';
import { KaryawanModule } from './karyawan/karyawan.module';
import { PelangganModule } from './pelanggan/pelanggan.module';
import { Pelanggan } from './pelanggan/entities/pelanggan.entity';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    TypeOrmModule.forFeature([User, Pelanggan]),
    KaryawanModule,
    PelangganModule,
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService]
})
export class UsersModule {}

