import { Controller, Get, Post, Body, Patch, Param, Delete, ParseUUIDPipe, HttpStatus, Put } from '@nestjs/common';
import { KaryawanService } from './karyawan.service';
import { CreateKaryawanDto } from './dto/create-karyawan.dto';
import { UpdateKaryawanDto } from './dto/update-karyawan.dto';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { Karyawan } from './entities/karyawan.entity';

@Controller('karyawan')
export class KaryawanController {
  constructor(private readonly karyawanService: KaryawanService) {}

  @Post()
  async create(@Body() createKaryawanDto: CreateKaryawanDto, userId: string) {
    try{
      return {
          data: await this.karyawanService.create(createKaryawanDto, userId),
          statusCode: HttpStatus.CREATED,
          message: 'success create account',
      };
  } catch(e){
      console.log(e);
      
      return e;
  }
  }

  @Get()
  async getAll(@Paginate() query: PaginateQuery): Promise<Paginated<Karyawan>>{
    try{
      return await this.karyawanService.findAll(query)
    } catch(e){
      console.log(e);

    }
  }

  @Get(':id')
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.karyawanService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put(':id')
  async update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateKaryawanDto:UpdateKaryawanDto,
  ) {
    return {
      data: await this.karyawanService.update(id,updateKaryawanDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete(':id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.karyawanService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
