import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";

export class CreateKaryawanDto {
    @ApiProperty()
    @IsOptional()
    name: string;

    @ApiProperty()
    @IsOptional()
    phone: string;

    @ApiProperty()
    @IsOptional()
    address: string;

    @ApiProperty()
    @IsOptional()
    image: string;

    @ApiProperty()
    @IsOptional()
    position: string;
}
