import { Module } from '@nestjs/common';
import { KaryawanService } from './karyawan.service';
import { KaryawanController } from './karyawan.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Karyawan } from './entities/karyawan.entity';
import { User } from '../entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Karyawan, User])],
  controllers: [KaryawanController],
  providers: [KaryawanService],
  exports: [KaryawanService]
})
export class KaryawanModule {}
