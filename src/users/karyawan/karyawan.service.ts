import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { EntityNotFoundError, Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { CreateKaryawanDto } from './dto/create-karyawan.dto';
import { UpdateKaryawanDto } from './dto/update-karyawan.dto';
import { Karyawan } from './entities/karyawan.entity';

@Injectable()
export class KaryawanService {
  constructor(
    @InjectRepository(Karyawan)
    private karyawanRepository: Repository<Karyawan>,
    @InjectRepository(User)
    private userRepository: Repository<User>
  ){}
  async create(data: any, userId: string) {
    const checkPhone = await this.karyawanRepository.findOne({
      where: {
        phone: data.phone,
      }
    });

    if(checkPhone) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_ACCEPTABLE,
          message: 'account is already exist'
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const karyawan = new Karyawan()
    karyawan.name = data.name
    karyawan.phone = data.phone
    karyawan.address = data.address
    karyawan.position = data.level
    karyawan.user = await this.userRepository.findOne({where: {id: userId}})    
    
    const result = await this.karyawanRepository.insert(karyawan)
    
    return this.karyawanRepository.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }

  async findAll (query: PaginateQuery): Promise<Paginated<Karyawan>>{
    return paginate(query, this.karyawanRepository, {
      sortableColumns: ['name'],
      defaultSortBy: [['name', 'ASC']],
      searchableColumns: ['name'],
      defaultLimit: 5,
      relations: ['user']
    })
  }


  async findOne(id: string) {
    try {
      return await this.karyawanRepository.findOneOrFail({
        where: {
          id,
        },
        relations: ['user']
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async update(id: string, updateKaryawanDto:UpdateKaryawanDto) {
    try {
      await this.karyawanRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    const karyawan = new Karyawan();
    karyawan.name = updateKaryawanDto.name
    karyawan.address = updateKaryawanDto.address
    karyawan.phone = updateKaryawanDto.phone
    karyawan.image = updateKaryawanDto.image
    karyawan.position = updateKaryawanDto.position
    await this.karyawanRepository.update(id, karyawan);

    return this.karyawanRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  async remove(id: string) {
    try {
      await this.karyawanRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.karyawanRepository.delete(id);
  }
}
