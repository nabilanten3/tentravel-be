import { User } from "src/users/entities/user.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn, VersionColumn } from "typeorm";

export enum Position {
    ADMIN = 'administrasi',
    OP = 'operator',
    OWNER = 'pemilik'
}

@Entity()
export class Karyawan {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @Column()
    address: string;

    @Column()
    phone: string;

    @Column({nullable:true})
    image: string;

    @Column()
    position: string;

    @ManyToOne(
        () => {
          return User
        },
        (user) => {
          return user.id
        }
      )
      user: User

    @CreateDateColumn({
        type: 'timestamp with time zone',
        nullable: false,
      })
      createdAt: Date;
    
      @UpdateDateColumn({
        type: 'timestamp with time zone',
        nullable: false,
      })
      updatedAt: Date;
    
      @DeleteDateColumn({
        type: 'timestamp with time zone',
        nullable: true,
      })
      deletedAt: Date;
    
      @VersionColumn({select: false})
      version: number;
}
