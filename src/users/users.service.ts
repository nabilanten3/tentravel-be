import { HttpException, HttpStatus, Injectable, BadRequestException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { EntityNotFoundError, Repository } from 'typeorm';
import { Level, User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as uuid from 'uuid';
import { hashPassword } from 'src/utils/hash-password';
import { FilterOperator, paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { UpdatePasswordDto } from './dto/update-password.dto';
import { Pelanggan } from './pelanggan/entities/pelanggan.entity';
import { PelangganService } from './pelanggan/pelanggan.service';
import { KaryawanService } from './karyawan/karyawan.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private pelangganService: PelangganService,
    private karyawanService: KaryawanService,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const checkEmail = await this.usersRepository.findOne({
      where: {
        email: createUserDto.email,
      }
    });

    if(checkEmail) {
      throw new HttpException(
        {
          statusCode: HttpStatus.NOT_ACCEPTABLE,
          message: 'account is already exist'
        },
        HttpStatus.BAD_REQUEST,
      );
    }

    const salt = uuid.v4();
    const user = new User();
    user.email =  createUserDto.email
    user.password = await hashPassword(createUserDto.password, salt)
    user.salt = salt
    user.level = createUserDto.level

    console.log(user);
    

    const result = await this.usersRepository.insert(user)
    
    if (user.level === "Pelanggan") {
      await this.pelangganService.create(createUserDto, result.identifiers[0].id)
    } else {
      await this.karyawanService.create(createUserDto, result.identifiers[0].id)
    }
    return this.usersRepository.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }

  async findAll (query: PaginateQuery): Promise<Paginated<User>>{
    return paginate(query, this.usersRepository, {
      sortableColumns: ['email'],
      defaultSortBy: [['email', 'ASC']],
      searchableColumns: ['email'],
      defaultLimit: 5,
    })
  }


  async findOne(id: string) {
    try {
      return await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async update(id: string, updateUserDto:UpdateUserDto) {
    try {
      await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    const user = new User();
    user.email = updateUserDto.email
    user.level = updateUserDto.level
    await this.usersRepository.update(id, user);

    return this.usersRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  async remove(id: string) {
    try {
      await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.usersRepository.delete(id);
  }

  async findByEmail(email:string) {
    try{
      return await this.usersRepository.findOneOrFail({
        where: {
          email,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
          throw new HttpException({
              statusCode: HttpStatus.NOT_FOUND,
              error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
      );
      } else {
          throw e;
      }
    }
  }
}
