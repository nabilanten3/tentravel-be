import { HttpStatus, Injectable } from '@nestjs/common';
import { HttpException, UnauthorizedException } from '@nestjs/common/exceptions';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt'
import { LoginDto } from 'src/users/dto/login.dto';
import { hashPassword } from 'src/utils/hash-password';
import { Karyawan } from 'src/users/karyawan/entities/karyawan.entity';
import { Pelanggan } from 'src/users/pelanggan/entities/pelanggan.entity';

@Injectable()
export class AuthService {
    constructor(
        private jwtService: JwtService,
        private userService: UsersService,
        @InjectRepository(User) private readonly repositoryUser: Repository<User>,
        @InjectRepository(Pelanggan) private readonly repositoryPelanggan: Repository<Pelanggan>,
        @InjectRepository(Karyawan) private readonly repositoryKaryawan: Repository<Karyawan>,
    ){}

    async validateUser(email: string, password: string): Promise<any> {
    const user = await this.userService.findByEmail(email);    

    if (user && user.password === (await hashPassword(password, user.salt))) {
      const { password, ...result } = user;

      return result;
    }

    return null;
  }

    async login(loginDto: LoginDto) {
      const user = await this.repositoryUser.findOne({where: {email: loginDto.email}})
      
      if (
        user.password !== (await hashPassword(loginDto.password, user.salt))
      ) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Incorrect Password',
          },
          HttpStatus.NOT_FOUND,
        );
      }

      if (!user) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: 'Your account is not active',
          },
          HttpStatus.NOT_FOUND,
        );
      }

      const payload = {
        id: user.id,
        email: user.email,
      };

      return {
        email: user.email,
        level: user.level,
        access_token: this.jwtService.sign(payload),
      };
    }

    async verfyJwt(jwt: string) {
      return this.jwtService.verifyAsync(jwt);
    }
}
