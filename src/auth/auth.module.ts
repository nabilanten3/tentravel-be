import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { PassportModule } from '@nestjs/passport';
import { forwardRef } from '@nestjs/common/utils';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { JwtStrategy } from './jwt.strategy';
import { LocalStrategy } from './local.strategy';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from './jwt-auth.guard';
import { Pelanggan } from 'src/users/pelanggan/entities/pelanggan.entity';
import { Karyawan } from 'src/users/karyawan/entities/karyawan.entity';
@Module({
  imports: [
    forwardRef(() => UsersModule),
    JwtModule.register({
      secret : 'asdfg',
      signOptions : {
        expiresIn : '2h'
      }
    }),
    TypeOrmModule.forFeature([User, Pelanggan, Karyawan]),
    PassportModule
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService]
})
export class AuthModule {}
