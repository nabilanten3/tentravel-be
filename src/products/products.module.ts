import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaketWisata } from './entities/paket-wisata.entity';
import { DaftarPaket } from './entities/wisata.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PaketWisata, DaftarPaket])],
  controllers: [ProductsController],
  providers: [ProductsService]
})
export class ProductsModule {}
