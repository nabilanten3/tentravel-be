import { PartialType } from '@nestjs/swagger';
import { CreatePaketWisataDto } from './create-paket-wisata';

export class UpdatePaketWisataDto extends PartialType(CreatePaketWisataDto) {}
