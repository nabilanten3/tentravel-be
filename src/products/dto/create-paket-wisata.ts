import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";

export class CreatePaketWisataDto {
    @ApiProperty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsString()
    description: string;

    @ApiProperty()
    @IsString()
    itinerary: string;

    @ApiProperty()
    @IsString()
    facility: string;

    @ApiProperty()
    @IsOptional()
    discount: number;

    @ApiProperty()
    @IsOptional()
    image: string[];
}
