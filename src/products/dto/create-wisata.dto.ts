import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class CreateWisataDto {
    @ApiProperty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsNumber()
    participants: number;

    @ApiProperty()
    @IsNumber()
    price: number;

    @ApiProperty()
    @IsNotEmpty()
    paketWisataId: string
}