import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { object } from 'joi';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { map } from 'rxjs';
import { EntityNotFoundError, Repository } from 'typeorm';
import { CreatePaketWisataDto } from './dto/create-paket-wisata';
import { CreateWisataDto } from './dto/create-wisata.dto';
import { UpdatePaketWisataDto } from './dto/update-paket-wisata.dto';
import { UpdateWisataDto } from './dto/update-wisata.dto';
import { PaketWisata } from './entities/paket-wisata.entity';
import { DaftarPaket } from './entities/wisata.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(PaketWisata)
    private paketWisataRepo: Repository<PaketWisata>,
    @InjectRepository(DaftarPaket)
    private wisataRepo: Repository<DaftarPaket>,
  ){}
  async createPaketWisata(createPaketWisataDto: CreatePaketWisataDto) {
    const paket = new PaketWisata()
    paket.name = createPaketWisataDto.name
    paket.description = createPaketWisataDto.description
    paket.facility = createPaketWisataDto.facility
    paket.image = createPaketWisataDto.image
    paket.discount = createPaketWisataDto.discount
    paket.itinerary = createPaketWisataDto.itinerary

    const result = await this.paketWisataRepo.insert(paket)
    return this.paketWisataRepo.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }

  async findAll (query: PaginateQuery): Promise<Paginated<PaketWisata>>{
    return paginate(query, this.paketWisataRepo, {
      sortableColumns: ['name'],
      defaultSortBy: [['name', 'ASC']],
      searchableColumns: ['name'],
      defaultLimit: 5,
    })
  }


  async findOne(id: string) {
    try {
      return await this.paketWisataRepo.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async update(id: string, updatePaketWisataDto:UpdatePaketWisataDto) {
    try {
      await this.paketWisataRepo.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    const paket = new PaketWisata()
    paket.name = updatePaketWisataDto.name
    paket.description = updatePaketWisataDto.description
    paket.facility = updatePaketWisataDto.facility
    paket.image = updatePaketWisataDto.image
    paket.discount = updatePaketWisataDto.discount
    paket.itinerary = updatePaketWisataDto.itinerary
    await this.paketWisataRepo.update(id, paket);

    return this.paketWisataRepo.findOneOrFail({
      where: {
        id,
      },
    });
  }

  async remove(id: string) {
    try {
      await this.paketWisataRepo.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.paketWisataRepo.delete(id);
  }

  async createWisata(createWisataDto: CreateWisataDto){
    const wisata = new DaftarPaket()
    wisata.name = createWisataDto.name
    wisata.participants = createWisataDto.participants
    wisata.price = createWisataDto.price
    wisata.paket = await this.paketWisataRepo.findOne({where: {id: createWisataDto.paketWisataId}})

    const result = await this.wisataRepo.insert(wisata)

    return this.wisataRepo.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
      relations: ['paket']
    });
  }

  async findAllWisata (query: PaginateQuery): Promise<Paginated<DaftarPaket>>{
    return paginate(query, this.wisataRepo, {
      sortableColumns: ['name'],
      defaultSortBy: [['name', 'ASC']],
      searchableColumns: ['name'],
      defaultLimit: 5,
      relations: ['paket']
    })
  }


  async findOneWisata(id: string) {
    try {
      return await this.wisataRepo.findOneOrFail({
        where: {
          id,
        },
        relations: ['paket']
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async updateWisata(id: string, updateWisataDto:UpdateWisataDto) {
    try {
      await this.wisataRepo.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    const wisata = new DaftarPaket()
    wisata.name = updateWisataDto.name
    wisata.participants = updateWisataDto.participants
    wisata.price = updateWisataDto.price
    wisata.paket = await this.paketWisataRepo.findOne({where: {id: updateWisataDto.paketWisataId}})
    console.log(wisata);
    
    await this.wisataRepo.update(id, wisata);

    return this.wisataRepo.findOneOrFail({
      where: {
        id,
      },
    });
  }

  async removeWisata(id: string) {
    try {
      await this.wisataRepo.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.wisataRepo.delete(id);
  }
}
