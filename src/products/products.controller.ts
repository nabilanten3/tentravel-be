import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, ParseUUIDPipe, Put } from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreatePaketWisataDto } from './dto/create-paket-wisata';
import { UpdatePaketWisataDto } from './dto/update-paket-wisata.dto';
import { CreateWisataDto } from './dto/create-wisata.dto';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { PaketWisata } from './entities/paket-wisata.entity';
import { DaftarPaket } from './entities/wisata.entity';
import { UpdateWisataDto } from './dto/update-wisata.dto';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Post('paket-wisata')
  async create(@Body() createPaketWisataDto: CreatePaketWisataDto) {
    try{
      return {
          data: await this.productsService.createPaketWisata(createPaketWisataDto),
          statusCode: HttpStatus.CREATED,
          message: 'success create package',
      };
  } catch(e){
      console.log(e);
      
      return e;
  }
  }

  @Get('/paket-wisata')
  async getAll(@Paginate() query: PaginateQuery): Promise<Paginated<PaketWisata>>{
    try{
      return await this.productsService.findAll(query)
    } catch(e){
      console.log(e);

    }
  }

  @Get('/paket-wisata/:id')
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.productsService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put('/paket-wisata/:id')
  async update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updatePaketWisataDto: UpdatePaketWisataDto,
  ) {
    return {
      data: await this.productsService.update(id, updatePaketWisataDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete('/paket-wisata/:id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.productsService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Post('daftar-paket')
  async createPaket(@Body() createWisataDto: CreateWisataDto) {
    try{
      return {
          data: await this.productsService.createWisata(createWisataDto),
          statusCode: HttpStatus.CREATED,
          message: 'success create package',
      };
  } catch(e){
      console.log(e);
      
      return e;
  }
  }

  @Get('/daftar-paket')
  async getAllPaket(@Paginate() query: PaginateQuery): Promise<Paginated<DaftarPaket>>{
    try{
      return await this.productsService.findAllWisata(query)
    } catch(e){
      console.log(e);

    }
  }

  @Get('/daftart-paket/:id')
  async findOnePaket(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.productsService.findOneWisata(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put('/daftar-paket/:id')
  async updatePaket(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateWisataDto: UpdateWisataDto,
  ) {
    return {
      data: await this.productsService.updateWisata(id, updateWisataDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete('/daftar-paket/:id')
  async removePaket(@Param('id', ParseUUIDPipe) id: string) {
    await this.productsService.removeWisata(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
