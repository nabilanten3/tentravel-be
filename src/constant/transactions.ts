export const TransactionStatus = {
    INIT: 'Dipesan',
    PAID: 'Dibayar',
    SUCCESS: 'Selesai'
}