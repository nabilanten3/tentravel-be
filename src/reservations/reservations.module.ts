import { Module } from '@nestjs/common';
import { ReservationsService } from './reservations.service';
import { ReservationsController } from './reservations.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reservation } from './entities/reservation.entity';
import { Pelanggan } from 'src/users/pelanggan/entities/pelanggan.entity';
import { DaftarPaket } from 'src/products/entities/wisata.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Reservation, Pelanggan, DaftarPaket, User])],
  controllers: [ReservationsController],
  providers: [ReservationsService]
})
export class ReservationsModule {}
