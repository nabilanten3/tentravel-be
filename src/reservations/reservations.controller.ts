import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, UseGuards, Request, ParseUUIDPipe, Put } from '@nestjs/common';
import { ReservationsService } from './reservations.service';
import { CreateReservationDto } from './dto/create-reservation.dto';
import { UpdateReservationDto } from './dto/update-reservation.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { Reservation } from './entities/reservation.entity';

@Controller('reservations')
export class ReservationsController {
  constructor(private readonly reservationsService: ReservationsService) {}

  @Post()
  async create(@Request() req, @Body() createReservationDto: CreateReservationDto) {
    try{
      console.log("ini req "+req);
      
      return {
          data: await this.reservationsService.create(createReservationDto),
          statusCode: HttpStatus.CREATED,
          message: 'success create package',
      };
  } catch(e){
      console.log(e);
      
      return e;
  }
  }

  @Get()
  async getAll(@Paginate() query: PaginateQuery): Promise<Paginated<Reservation>>{
    try{
      return await this.reservationsService.findAll(query)
    } catch(e){
      console.log(e);

    }
  }

  @Get('/:id')
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.reservationsService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Post('/paid')
  async updateStatusPaid(
    @Body() updateReservationDto: UpdateReservationDto,
  ) {
    return {
      data: await this.reservationsService.statusPaid(updateReservationDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Post('/success')
  async updateStatusSuccess(
    @Body() updateReservationDto: UpdateReservationDto,
  ) {
    return {
      data: await this.reservationsService.statusSuccess(updateReservationDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete('/paket-wisata/:id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.reservationsService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

}
