import { PaketWisata } from "src/products/entities/paket-wisata.entity";
import { DaftarPaket } from "src/products/entities/wisata.entity";
import { User } from "src/users/entities/user.entity";
import { Pelanggan } from "src/users/pelanggan/entities/pelanggan.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Reservation {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    date: Date;

    @Column()
    price: number;

    @Column()
    number_of_participants: number;

    @Column()
    discount: number;

    @Column()
    discount_value: number;

    @Column()
    total: number;

    @Column()
    image: string;

    @Column()
    status: string;

    @ManyToOne(
        () => {
          return Pelanggan
        },
        (pelanggan) => {
          return pelanggan.id
        }
      )
      pelanggan: Pelanggan

      @ManyToOne(
        () => {
          return DaftarPaket
        },
        (daftar) => {
          return daftar.id
        }
      )
      paket: DaftarPaket

    @CreateDateColumn({
        type: 'timestamp with time zone',
        nullable: false,
      })
      createdAt: Date;
    
      @UpdateDateColumn({
        type: 'timestamp with time zone',
        nullable: false,
      })
      updatedAt: Date;
    
      @DeleteDateColumn({
        type: 'timestamp with time zone',
        nullable: true,
      })
      deletedAt: Date;
}
