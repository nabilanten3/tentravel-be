import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { TransactionStatus } from 'src/constant/transactions';
import { DaftarPaket } from 'src/products/entities/wisata.entity';
import { User } from 'src/users/entities/user.entity';
import { Pelanggan } from 'src/users/pelanggan/entities/pelanggan.entity';
import { EntityNotFoundError, Repository } from 'typeorm';
import { CreateReservationDto } from './dto/create-reservation.dto';
import { UpdateReservationDto } from './dto/update-reservation.dto';
import { Reservation } from './entities/reservation.entity';

@Injectable()
export class ReservationsService {
  constructor(
    @InjectRepository(DaftarPaket)
    private daftarPaketRepo: Repository<DaftarPaket>,
    @InjectRepository(Reservation)
    private reservationRepo: Repository<Reservation>,
    @InjectRepository(Pelanggan)
    private pelangganRepo: Repository<Pelanggan>,
    @InjectRepository(User)
    private userRepo: Repository<User>,
  ) {}
  async create(createReservationDto: CreateReservationDto) {
    const product = await this.daftarPaketRepo.findOne({where: {id: createReservationDto.daftarPaketId}, relations:['paket']})
    const reservation = new Reservation()
    reservation.date = createReservationDto.reservationDate
    reservation.discount = product.paket.discount
    reservation.number_of_participants = product.participants
    reservation.image = createReservationDto.image
    reservation.paket = product
    reservation.price = product.price
    reservation.pelanggan = await this.pelangganRepo.findOne({where: {id: createReservationDto.pelangganId}})
    reservation.discount_value = reservation.discount * reservation.price
    reservation.total = reservation.price - reservation.discount_value
    reservation.status = TransactionStatus.INIT

    console.log(reservation);
    

    const result = await this.reservationRepo.insert(reservation)

    return this.reservationRepo.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
      relations: ['paket', 'pelanggan']
    });
  }

  async findAll (query: PaginateQuery): Promise<Paginated<Reservation>>{
    return paginate(query, this.reservationRepo, {
      sortableColumns: ['date'],
      defaultSortBy: [['date', 'DESC']],
      searchableColumns: ['date'],
      defaultLimit: 5,
      relations: ['paket', 'pelanggan']
    })
  }
  async findOne(id: string) {
    try {
      return await this.reservationRepo.findOneOrFail({
        where: {
          id,
        },
        relations: ['paket', 'pelanggan']
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async statusPaid(updateReservationDto: UpdateReservationDto) {
    try {
      await this.reservationRepo.findOneOrFail({
        where: {
          id: updateReservationDto.id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    const reservation = new Reservation()
    reservation.status = TransactionStatus.SUCCESS
    await this.reservationRepo.update(updateReservationDto, reservation);

    return this.reservationRepo.findOneOrFail({
      where: {
        id: updateReservationDto.id,
      },
    });
  }

  async statusSuccess(updateReservationDto: UpdateReservationDto) {
    try {
      await this.reservationRepo.findOneOrFail({
        where: {
          id: updateReservationDto.id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    const reservation = new Reservation()
    reservation.status = TransactionStatus.SUCCESS
    await this.reservationRepo.update(updateReservationDto, reservation);

    return this.reservationRepo.findOneOrFail({
      where: {
        id: updateReservationDto.id,
      },
    });
  }

  async remove(id: string) {
    try {
      await this.reservationRepo.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.reservationRepo.delete(id);
  }
}
