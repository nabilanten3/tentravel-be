import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class CreateReservationDto {
    @ApiProperty()
    @IsNotEmpty()
    pelangganId: string;

    @ApiProperty()
    @IsNotEmpty()
    daftarPaketId: string;

    @ApiProperty()
    @IsNotEmpty()
    reservationDate: Date;

    @ApiProperty()
    @IsNotEmpty()
    image: string;
}
